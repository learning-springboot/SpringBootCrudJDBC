package com.learning.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learning.model.User;
import com.learning.repository.UserRepository;

@Service
public class UserService 
{
	@Autowired
	private UserRepository userRepository;

	/*
	 * findAll() Method
	 * Used to find all entries from database
	 */
	public List<User> getAllUsers()  
	{    
		List<User> userList = new ArrayList<>();    
		userRepository.findAll().forEach(userList::add);    
		return userList;    
	}   
	
	/*
	 * findById() Method
	 * Used to find an entry by Id from database
	 */
	public User getUserById(Long Id)  
	{    
		User course = userRepository.findById(Id).get();
		return course;    
	} 
	
	/*
	 * save() Method
	 * Used to Create new Entry in database
	 */
	public void createUser(User userList)  
	{    
		userRepository.save(userList);    
	}	
	
	/*
	 * update() Method
	 * Used to Update an existing entry in database 
	 */
	public void updateUser(User userList)  
	{    
		userRepository.update(userList);    
	}		
	
	/*
	 * deleteById() Method
	 * Used to Delete existing Entry by Id in database
	 */
	public void deleteUserById(Long userId)  
	{    
		userRepository.deleteById(userId);   
	}	
	
	/*
	 * deleteAll() Method
	 * Used to Delete all existing entries in database
	 */
	public void deleteAllUsers()  
	{    
		userRepository.deleteAll();  
	}	
}
