package com.learning.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learning.model.Course;
import com.learning.repository.CourseRepository;

@Service
public class CourseService 
{
	@Autowired
	private CourseRepository courseRepository;

	/*
	 * CrudRepository findAll() Method
	 * Used to find all entries from database
	 */
	public List<Course> getAllCourses()  
	{    
		List<Course> courseList = new ArrayList<>();    
		courseRepository.findAll().forEach(courseList::add);    
		return courseList;    
	}   
	
	/*
	 * CrudRepository findById() Method
	 * Used to find an entry by Id from database
	 */
	public Course getCourseById(String Id)  
	{    
		Course course = courseRepository.findById(Id).get();
		return course;    
	} 
	
	/*
	 * CrudRepository save() Method
	 * Used to Create new Entry in database
	 * Used to Update an existing entry in database 
	 */
	public void createUpdateCourse(Course courseList)  
	{    
		courseRepository.save(courseList);    
	}	
	
	/*
	 * CrudRepository deleteById() Method
	 * Used to Delete existing Entry by Id in database
	 */
	public void deleteCourseById(String courseId)  
	{    
		courseRepository.deleteById(courseId);   
	}	
	
	/*
	 * CrudRepository deleteAll() Method
	 * Used to Delete all existing entries in database
	 */
	public void deleteAllCourses()  
	{    
		courseRepository.deleteAll();  
	}	
}
