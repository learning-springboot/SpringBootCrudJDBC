package com.learning.repository;

import java.util.List;
import java.util.Optional;

import com.learning.model.User;

public interface UserRepository 
{
    int count();

    int save(User user);

    int update(User user);

    int deleteById(Long userId);
    
    int deleteAll();

    List<User> findAll();

    List<User> findByName(String userName);

    Optional<User> findById(Long userId);

    String getNameById(Long userId);

}
