package com.learning.repository;

import com.learning.model.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository 
			extends CrudRepository<Course, String>   
{

}


