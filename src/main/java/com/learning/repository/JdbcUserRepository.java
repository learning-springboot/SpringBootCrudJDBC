package com.learning.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.learning.model.User;

@Repository
public class JdbcUserRepository
					implements UserRepository
{
    @Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public int count() 
	{
        return jdbcTemplate
                .queryForObject("SELECT COUNT(*) FROM USER ", Integer.class);
	}

	@Override
	public int save(User user) 
	{
        return jdbcTemplate.update(
                "INSERT INTO USER (USERID , USERNAME ) VALUES (?,?)",
                	user.getUserId(), user.getUserName());
	}

	@Override
	public int update(User user) {
	       return jdbcTemplate.update(
	                "update USER set USERNAME = ? where USERID = ?",
	                	user.getUserName(), user.getUserId());
	}

	@Override
	public int deleteById(Long userId) {
        return jdbcTemplate.update(
                "delete USER where USERID = ?",
                	userId);
	}
	
	@Override
	public int deleteAll() {
        return jdbcTemplate.update(
                "DELETE FROM USER ");
	}

	@Override
	public List<User> findAll() 
	{
        return jdbcTemplate.query(
                " SELECT * FROM USER",
                (rs, rowNum) ->
                        new User(
                                rs.getLong("USERID"),
                                rs.getString("USERNAME")
                        )
        );
	}
			
	@Override
	public Optional<User> findById(Long userId) {
        return jdbcTemplate.queryForObject(
                "select * from USER where USERID = ?",
	                new Object[]{userId},
	                (rs, rowNum) ->
	                        Optional.of(new User(
	                                rs.getLong("USERID"),
	                                rs.getString("USERNAME")
	                        ))
        		);
	}			

	@Override
	public List<User> findByName(String name) {
        return jdbcTemplate.query(
                "select * from USER where USERNAME like ? ",
                new Object[]{"%" + name },
                (rs, rowNum) ->
                        new User(
                                rs.getLong("USERID"),
                                rs.getString("USERNAME")
                        )
        );
	}



	@Override
	public String getNameById(Long userId) {
        return jdbcTemplate.queryForObject(
                "select USERNAME from USER where USERID = ?",
                new Object[]{userId},
                String.class
        );
	}

}
