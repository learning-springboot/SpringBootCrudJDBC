package com.learning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.learning.model.User;
import com.learning.service.UserService;

@RestController
public class UserController 
{
	   
   @Autowired 
   private UserService userService;
   
   @GetMapping("/User")
   public ResponseEntity<Object> getAllUsers() 
   {
	   List<User> userList = userService.getAllUsers(); 
	   return new ResponseEntity<>(userList, HttpStatus.OK);
   }
   
   @GetMapping("/User/{id}")
   public ResponseEntity<Object> getUserById(@PathVariable("id") Long id)  
   {
	   User user = userService.getUserById(id); 
	   return new ResponseEntity<>(user, HttpStatus.OK);
   }
   
   @PostMapping("/createUser")
   public ResponseEntity<Object> createUser(@RequestBody User User) 
   {
	  userService.createUser(User);
      return new ResponseEntity<>("User created", HttpStatus.CREATED);
   }    
   
   @PostMapping("/updateUser")
   public ResponseEntity<Object> updateUser(@RequestBody User User) 
   {
	  userService.updateUser(User);
      return new ResponseEntity<>("User updated", HttpStatus.CREATED);
   }   

   @DeleteMapping("/User/{id}")
   public ResponseEntity<Object> deleteUserById(@PathVariable("id") Long id) 
   {  
	  userService.deleteUserById(id);
      return new ResponseEntity<>("User " + id + " deleted", HttpStatus.OK);
   }  
   
   @DeleteMapping("/User")
   public ResponseEntity<Object> deleteAllUsers() 
   {  
	  userService.deleteAllUsers();
      return new ResponseEntity<>("All User deleted", HttpStatus.OK);
   }    
}
