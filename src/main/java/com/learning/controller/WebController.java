package com.learning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.learning.model.Course;
import com.learning.service.CourseService;


@Controller 
public class WebController 
{
	@Autowired 
	private CourseService courseService;
	
	@GetMapping("/index")
	public String index() 
	{
		return "index";
	}	
   
	@GetMapping("/input")
	public String input() 
	{
		return "input";
	}	   
   
	@PostMapping("/save")
	public ModelAndView save(@ModelAttribute Course course)  
	{    
		ModelAndView modelAndView = new ModelAndView();    
		modelAndView.setViewName("course-data");        
		modelAndView.addObject("course", course);      

		courseService.createUpdateCourse(course);
	   
		return modelAndView;    
	}    
}

