package com.learning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.learning.model.Course;
import com.learning.service.CourseService;

@RestController
public class CourseControllerV3 
{
	   
   @Autowired 
   private CourseService courseService;
   
   @GetMapping("/CoursesV3")
   public ResponseEntity<Object> getAllCourses() 
   {
	   List<Course> courseList = courseService.getAllCourses(); 
	   return new ResponseEntity<>(courseList, HttpStatus.OK);
   }
   
   @GetMapping("/CoursesV3/{id}")
   public ResponseEntity<Object> getCourseById(@PathVariable("id") String id)  
   {
	   Course course = courseService.getCourseById(id); 
	   return new ResponseEntity<>(course, HttpStatus.OK);
   }
   
   @PostMapping("/CoursesV3")
   public ResponseEntity<Object> createUpdateCourse(@RequestBody Course Course) 
   {
	  courseService.createUpdateCourse(Course);
      return new ResponseEntity<>("Course created", HttpStatus.CREATED);
   }    

   @DeleteMapping("/CoursesV3/{id}")
   public ResponseEntity<Object> deleteCourseById(@PathVariable("id") String id) 
   {  
	  courseService.deleteCourseById(id);
      return new ResponseEntity<>("Course " + id + " deleted", HttpStatus.OK);
   }  
   
   @DeleteMapping("/CoursesV3")
   public ResponseEntity<Object> deleteAllCourses() 
   {  
	  courseService.deleteAllCourses();
      return new ResponseEntity<>("All Course deleted", HttpStatus.OK);
   }    
}
